# Jaco-Witze
Böse Witze. Sie sind weder jugendfrei noch politisch korrekt.

"Wie bekommt man 11 Millionen Follower? Man läuft mit einer Wasserflasche durch Afrika." \
"Was ist blau und orange und liegt auf dem Boden eines Pools? Ein Baby mit geplatzten Schwimmflügeln." \
"Schwarzer Humor ist wie was zu Essen. Hat nicht jeder."

<br>

What **you can learn** in this skill:
* Basic skill setup
* Read texts from files

**Complexity**: Very easy

<br>

Run skill solely for debugging purposes: \
(Assumes mqtt-broker already running) 
```bash
docker run --network host --rm \
  --volume `pwd`/skills/skills/Jaco-Witze/:/Jaco-Master/skills/skills/Jaco-Witze/:ro \
  --volume `pwd`/skills/skills/Jaco-Witze/skilldata/:/Jaco-Master/skills/skills/Jaco-Witze/skilldata/ \
  --volume `pwd`/../jacolib/:/Jaco-Master/jacolib/:ro \
  --volume `pwd`/userdata/config/:/Jaco-Master/userdata/config/:ro \
  -it master_base_image_amd64 python3 /Jaco-Master/skills/skills/Jaco-Witze/action-jokes.py
```

### Joke Sources
**German**
- https://www.witzepause.com
- https://schlechtewitze.com
- https://github.com/pyjokes/pyjokes

**English**
- Translated from german
